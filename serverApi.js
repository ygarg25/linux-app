var express = require("express");
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept"
  );
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  res.setHeader("Access-Control-Expose-Headers", "X-Auth-Token");
  next();
});

const port = 2410;

app.listen(port, () => console.log("listening on port", port));

const { exec } = require("child_process");

// exec("ls -la", (error, stdout, stderr) => {
//   if (error) {
//     console.log(`error: ${error.message}`);
//     return;
//   }
//   if (stderr) {
//     console.log(`stderr: ${stderr}`);
//     return;
//   }
//   console.log(`stdout: ${stdout}`);
// });

app.get("/commandRun/:cmd", function (req, res) {
  let cmd = req.params.cmd;
  console.log("command run", cmd);
  exec(`${cmd}`, (error, stdout, stderr) => {
    if (error) {
      console.log(`error: ${cmd},${error.message}`);
      res.send({ data: error.message });
      return;
    }
    if (stderr) {
      console.log(`stderr: ${cmd},${stderr}`);
      res.send({ data: stderr });
      return;
    }
    console.log(`stdout:${cmd}, ${stdout}`);
    res.send({ data: stdout });
  });
});
