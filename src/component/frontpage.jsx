import React, { Component } from "react";
import axios from "axios";

class FrontPage extends Component {
  state = {
    command: { cmd: "", output: "" },
    errors: {},
    view: "bottom",
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    let errors = this.validate();
    this.setState({ errors: errors });
    let errCount = Object.keys(errors).length;
    console.log(errors, errCount);
    if (errCount > 0) return;
    console.log("submitted : ", this.state.customer);
    const { data: response } = await axios.get(
      "http://localhost:2410/commandRun/" + this.state.command.cmd
    );
    //alert("Successfully Submited");

    const command = { ...this.state.command };
    command.output = response.data;
    console.log("dddd", response);
    this.setState({ command: command });
  };

  validate = () => {
    let errs = {};
    if (!this.state.command.cmd.trim()) {
      errs.cmd = "Command is required";
    }
    return errs;
  };

  isFormValid = () => {
    let errs = this.validate();
    let errCount = Object.keys(errs).length;
    console.log("x", errCount);
    return errCount > 0;
  };

  validateInput = (e) => {
    switch (e.currentTarget.name) {
      case "name":
        if (!e.currentTarget.value.trim()) return "command is required";
        break;
      default:
        break;
    }
    return "";
  };

  handleChange = (e) => {
    let errString = this.validateInput(e);
    const errors = { ...this.state.errors };
    errors[e.currentTarget.name] = errString;
    const { currentTarget: input } = e;
    const command = { ...this.state.command };
    command[input.name] = input.value;
    this.setState({ command: command, errors: errors });
  };

  handleView = (v) => {
    this.setState({ view: v });
  };
  render() {
    const { command, view } = this.state;
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          {view === "bottom" && (
            <div>
              <div className="form-group">
                <label htmlFor="name">Enter Command</label>
                <input
                  value={command.cmd}
                  onChange={this.handleChange}
                  type="text"
                  id="cmd"
                  name="cmd"
                  placeholder="Enter command"
                  className="form-control"
                ></input>
                {this.state.errors.cmd ? (
                  <div className="alert alert-danger">
                    {this.state.errors.cmd}
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="form-group">
                <label htmlFor="age">Output</label>
                <input
                  value={command.output}
                  //   onChange={this.handleChange}
                  type="text"
                  id="output"
                  name="output"
                  placeholder=""
                  className="form-control"
                ></input>
                {this.state.errors.output ? (
                  <div className="alert alert-danger">
                    {this.state.errors.output}
                  </div>
                ) : (
                  ""
                )}
              </div>
              <button
                className="btn btn-primary m-2"
                disabled={this.isFormValid()}
              >
                Submit
              </button>
            </div>
          )}
          {view === "right" && (
            <div className="row">
              <div className="form-group col-5 ">
                <label htmlFor="name">Enter Command</label>
                <input
                  value={command.cmd}
                  onChange={this.handleChange}
                  type="text"
                  id="cmd"
                  name="cmd"
                  placeholder="Enter command"
                  className="form-control"
                ></input>
                {this.state.errors.cmd ? (
                  <div className="alert alert-danger">
                    {this.state.errors.cmd}
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="form-group col-5">
                <label htmlFor="age">Output</label>
                <input
                  value={command.output}
                  //   onChange={this.handleChange}
                  type="text"
                  id="output"
                  name="output"
                  placeholder=""
                  className="form-control"
                ></input>
                {this.state.errors.output ? (
                  <div className="alert alert-danger">
                    {this.state.errors.output}
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="col-2 mt-3">
                <button
                  className="btn btn-primary m-2"
                  disabled={this.isFormValid()}
                >
                  Submit
                </button>
              </div>
            </div>
          )}
          {view === "left" && (
            <div className="row">
              <div className="form-group col-5">
                <label htmlFor="age">Output</label>
                <input
                  value={command.output}
                  //   onChange={this.handleChange}
                  type="text"
                  id="output"
                  name="output"
                  placeholder=""
                  className="form-control"
                ></input>
                {this.state.errors.output ? (
                  <div className="alert alert-danger">
                    {this.state.errors.output}
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className="col-2 mt-3">
                <button
                  className="btn btn-primary m-2"
                  disabled={this.isFormValid()}
                >
                  Submit
                </button>
              </div>{" "}
              <div className="form-group col-5 ">
                <label htmlFor="name">Enter Command</label>
                <input
                  value={command.cmd}
                  onChange={this.handleChange}
                  type="text"
                  id="cmd"
                  name="cmd"
                  placeholder="Enter command"
                  className="form-control"
                ></input>
                {this.state.errors.cmd ? (
                  <div className="alert alert-danger">
                    {this.state.errors.cmd}
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          )}
          {view === "top" && (
            <div className="">
              <div className="form-group ">
                <label htmlFor="age">Output</label>
                <input
                  value={command.output}
                  //   onChange={this.handleChange}
                  type="text"
                  id="output"
                  name="output"
                  placeholder=""
                  className="form-control"
                ></input>
                {this.state.errors.output ? (
                  <div className="alert alert-danger">
                    {this.state.errors.output}
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div className=" mt-3">
                <button
                  className="btn btn-primary m-2"
                  disabled={this.isFormValid()}
                >
                  Submit
                </button>
              </div>{" "}
              <div className="form-group ">
                <label htmlFor="name">Enter Command</label>
                <input
                  value={command.cmd}
                  onChange={this.handleChange}
                  type="text"
                  id="cmd"
                  name="cmd"
                  placeholder="Enter command"
                  className="form-control"
                ></input>
                {this.state.errors.cmd ? (
                  <div className="alert alert-danger">
                    {this.state.errors.cmd}
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          )}
        </form>
        <div>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.handleView("left")}
          >
            Left
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.handleView("right")}
          >
            Right
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.handleView("top")}
          >
            Top
          </button>
          <button
            className="btn btn-primary m-2"
            onClick={() => this.handleView("bottom")}
          >
            Bottom
          </button>
        </div>
      </div>
    );
  }
}

export default FrontPage;
